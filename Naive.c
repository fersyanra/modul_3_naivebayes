#include <stdio.h>
#include <conio.h>
#include <math.h>
#include "boolean.h"
#define nMax 10

typedef struct {
	int Jkel;			//pria = 1		wanita = 2
	int Pend;			//SMA = 1		S1 = 2		S2 = 3
	int Bidang;			//Pendidikan = 1	 Marketing = 2		Wirausaha = 3		Profesional = 4
	int Usia;			//Muda = 1		Tua = 2
	boolean Kredit;
} PKK;

/*typedef struct {
	PKK kartu[nMax];
	int nEff;
} TabPKK;
*/




int main() {
	
	//DATA AWAL
		//kamus
		PKK T[8];
		
		//algo
			T[0].Jkel = 2;
			T[0].Pend = 3;
			T[0].Bidang = 1;
			T[0].Usia = 2;
			T[0].Kredit = true;
			
			T[1].Jkel = 1;
			T[1].Pend = 2;
			T[1].Bidang = 2;
			T[1].Usia = 1;
			T[1].Kredit = true;
			
			T[2].Jkel = 2;
			T[2].Pend = 1;
			T[2].Bidang = 3;
			T[2].Usia = 2;
			T[2].Kredit = true;
			
			T[3].Jkel = 1;
			T[3].Pend = 2;
			T[3].Bidang = 4;
			T[3].Usia = 2;
			T[3].Kredit = true;
			
			T[4].Jkel = 1;
			T[4].Pend = 3;
			T[4].Bidang = 4;
			T[4].Usia = 1;
			T[4].Kredit = false;
			
			T[5].Jkel = 1;
			T[5].Pend = 1;
			T[5].Bidang = 3;
			T[5].Usia = 1;
			T[5].Kredit = false;
			
			T[6].Jkel = 2;
			T[6].Pend = 1;
			T[6].Bidang = 2;
			T[6].Usia = 1;
			T[6].Kredit = false;
			
			T[7].Jkel = 2;
			T[7].Pend = 2;
			T[7].Bidang = 1;
			T[7].Usia = 2;
			T[7].Kredit = false;
			
			
		printf("|Jenis Kelamin	| Pendidikan	|	Bidang Pekerjaan	| Kelompok Usia	| Kartu Kredit	|\n");
		printf("|Wanita		|S2		|Pendidikan			|Tua		|True		|\n");
		printf("|Pria		|S1		|Marketing			|Muda		|True		|\n");
		printf("|Wanita		|SMA		|Wirausaha			|Tua		|True		|\n");
		printf("|Pria		|S1		|Profesional			|Tua		|True		|\n");
		printf("|Pria		|S2		|Profesional			|Muda		|False		|\n");
		printf("|Pria		|SMA		|Wirausaha			|Muda		|False		|\n");
		printf("|Wanita		|SMA		|Marketing			|Muda		|False		|\n");
		printf("|Wanita		|S1		|Marketing			|Tua		|False		|\n\n");		
			
			
		printf("\n\n Soal: Wanita, SMA, Profesional, Tua\n\n");			
			
	//NILAI TRUE
		//kamus
		
			int i;
			
		//algo		
			
			//totaltrue
			float jumlah = 0;
			float totaltrue;
			
			for(i=0; i<8; i++){
				if(T[i].Kredit == 1){
					jumlah = jumlah +1;
				}				
			}
			
			totaltrue = jumlah/8;
			
			printf("\nTotalTrue: %0.2f", totaltrue);
			
			
			//Jkel true
			float jumkel = 0;
			float keltrue;
			
			for(i=0; i<8; i++){
				if(T[i].Jkel == 2 && T[i].Kredit == 1){
					jumkel = jumkel +1;
				}				
			}
			
			keltrue = jumkel/jumlah;
			printf("\nJenis kelamin true: %0.2f", keltrue);
			
			
			//pend true
			float jumpend = 0;
			float pendtrue;
			
			for(i=0; i<8; i++){
				if(T[i].Pend == 1 && T[i].Kredit == 1){
					jumpend = jumpend +1;
				}				
			}
			
			pendtrue = jumpend/jumlah;
			printf("\nPendidikan True: %0.2f", pendtrue);
			
			
			//Bidang true
			float jumbid = 0;
			float bidtrue;
			
			for(i=0; i<8; i++){
				if(T[i].Bidang == 4 && T[i].Kredit == 1){
					jumbid = jumbid +1;
				}				
			}
			
			bidtrue = jumbid/jumlah;
			printf("\nBidang Pekerjaan True: %0.2f", bidtrue);
			
			
			//Usia true
			float jumusia = 0;
			float usiatrue;
			
			for(i=0; i<8; i++){
				if(T[i].Usia == 2 && T[i].Kredit == 1){
					jumusia = jumusia +1;
				}				
			}
			
			usiatrue = jumusia/jumlah;
			printf("\nUsiaTrue: %0.2f", usiatrue);
			
		
	//NILAI FALSE
		//kamus
		
			
			
		//algo		
			
			//totalfalse
			float jumfalse = 0;
			float totalfalse;
			
			for(i=0; i<8; i++){
				if(T[i].Kredit == 0){
					jumfalse = jumfalse +1;
				}				
			}
			
			totalfalse = jumfalse/8;
			
			printf("\n\n\nTotalfalse: %0.2f", totalfalse);
			
			
			//Jkel false
			
			float kelfalse;
			
			for(i=0; i<8; i++){
				if(T[i].Jkel == 2 && T[i].Kredit == 0){
					jumkel = jumkel +1;
				}				
			}
			
			kelfalse = jumkel/jumfalse;
			printf("\nJenis kelamin false: %0.2f", kelfalse);
			
			
			//pend false
			
			float pendfalse;
			
			for(i=0; i<8; i++){
				if(T[i].Pend == 1 && T[i].Kredit == 0){
					jumpend = jumpend +1;
				}				
			}
			
			pendfalse = jumpend/jumfalse;
			printf("\nPendidikan false: %0.2f", pendfalse);
			
			
			//Bidang true
			
			float bidfalse;
			
			for(i=0; i<8; i++){
				if(T[i].Bidang == 4 && T[i].Kredit == 0){
					jumbid = jumbid +1;
				}				
			}
			
			bidfalse = jumbid/jumfalse;
			printf("\nBidang pekerjaan false: %0.2f", bidfalse);
			
			
			//Usia false
			
			float usiafalse;
			
			for(i=0; i<8; i++){
				if(T[i].Usia == 2 && T[i].Kredit == 0){
					jumusia = jumusia +1;
				}				
			}
			
			usiafalse = jumusia/jumfalse;
			printf("\nUsia false: %0.2f", usiafalse);	
	
	
			
	
	//NAIVE BAYES TRUE
		float naivetrue;
		
		naivetrue = totaltrue*keltrue*pendtrue*bidtrue*usiatrue;
		printf("\n\n\nNaive Bayes TRUE: %0.2f\n", naivetrue);
		
	//NAIVE BAYES FALSE
		float naivefalse;
		
		naivefalse = totalfalse*kelfalse*pendfalse*bidfalse*usiafalse;
		printf("Naive Bayes FALSE: %0.2f\n", naivefalse);

	
	
	// HASIL AKHIR NAIVE BAYES
		float naivetotal;
		
		naivetotal = (naivetrue/(naivetrue+naivefalse)) * 100;
		printf("\n\nNAIVE BAYES : %0.2f", naivetotal);
		


	
return 0;
}
